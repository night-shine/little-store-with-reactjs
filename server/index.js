const cors = require('cors')
const express = require("express");
const app = express()
const port = 5000

const database = require('./server-modules/dataManagager.js')

app.use(cors())
app.use(express.json())

app.post('/checkUser', (req, res) => {
    async function userCheck (props) {
        let response = await database.getData(props)
        res.json(response)
    }
    userCheck(req.body)
})
app.post('/createUser', (req, res) => {
    async function userCreate (props) {
        let response = await database.inserData(props)
        res.send(response)
    }
    userCreate(req.body)
})

app.listen(port, () => {
    console.log(`server is online on port: ${port}`)
})
