create table users(
    user_id int auto_increment PRIMARY KEY,
    user_email varchar(255) not null,
    user_name varchar(255) not null,
    user_pass varchar(255) not null,
    creation varchar(255)
);
-- terminal paste: 
-- create table users(user_id int auto_increment PRIMARY KEY, user_email varchar(255) not null, user_name varchar(255) not null, user_pass varchar(255) not null, creation varchar(255));