const database = require("mariadb")


/*
Example of the data
{
  user_id: 1,
  user_email: 'test@test.teste',
  user_name: 'teste',
  user_pass: 'teste',
  creation: 'Mon Sep 19 2022 18:55:19 GMT-0300 (Brasilia Standard Time)'
}
*/


const config = database.createPool({
    database: "store_db",
    user: "store",
    password: "weakpass"
})


// insert all the user creation data
async function inserData(data) {
    console.log(data)
    const connection = await config.getConnection()

    let check = await connection.query(`SELECT user_name FROM users WHERE user_email='${data.user_email}'`)
    
    if (check[0] === undefined) {
        await connection.query(`INSERT INTO users (user_email, user_name, user_pass, creation) VALUES ('${data.user_email}', '${data.user_name}', '${data.user_pass}', '${data.creation}')`)
        return {
            "status": "successful"
        }

    } else {
        return {
            "status": "failed",
            "reason": "user exists"
        }
    }
}


// return a request from the database, the data is the email and password
async function getData(data) {
    const connection = await config.getConnection()

    let dataRes = await connection.query(`select * from users where user_email='${data.user_email}'`)

    try {
        if (dataRes[0].user_pass !== data.user_pass) {
            return {
                "login": "failed",
                "reson": "password dont match"
            }
        }
    } catch {
        return {
            "login": "failed",
            "reson": "user don't existis"
        }
    }
    return {
        "login": "successful"
    }
}

module.exports = { getData, inserData }