# this is the server of the aplication

> **For better visualization, mermaid is required.**

> this server uses MariaDB for the database.

```mermaid

flowchart TD;

    subgraph Server;
        datamanager[Manager Data Base];
        getdata(get data);
        insertdata(insert data);
        database[(DataBase)]

        datamanager <-..-> getdata & insertdata;

        getdata & insertdata <-.-> database
    end;
    
    client([client]) <-. request / response ..-> Server;
    
```

---

## this is a example of request for checking the user

```js
{
    user_email: 'test@test.teste'
}
```

---

## this is a example of request to register an user

```js
{
  user_email: 'test@test.teste',
  user_name: 'teste',
  user_pass: 'teste',
  creation: 'Mon Sep 19 2022 18:55:19 GMT-0300 (Brasilia Standard Time)'
}
```

---

## this is a example of response

```js
{
  user_id: 1,
  user_email: 'test@test.teste',
  user_name: 'teste',
  user_pass: 'teste',
  creation: 'Mon Sep 19 2022 18:55:19 GMT-0300 (Brasilia Standard Time)'
}
```