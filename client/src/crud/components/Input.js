import React from "react";
import "../user-style.css"


const Input = (props) => {
  let elemConf = props.config
  return (
    <div className="inputContent">
      <label htmlFor={elemConf.label} className="crud-label">{elemConf.value}</label>
      <input id={elemConf.id} onChange={props.func} className="crud-input" type={elemConf.type} pattern={props.pattern} required />
    </div>
  )
}

export default Input;