import { React, useState } from "react";
import Input from "./components/Input";
import './user-style.css'
import profile from './components/profileModule.json'



const NewUser = () => {

    const [userInfo, setUserInfo] = useState({})
    const [uiChanger, setUi] = useState('Sign Up')
    const [page, setpage] = useState(profile.oldUser)



    const serverRequest = async (props) => {
        const server_exec = {
            'Sign Up': '/checkUser',
            'Sign In': '/createUser'
        }
        let reqConfig = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(props)
        }
        console.log(reqConfig)
        let response = await (await fetch(server_exec[uiChanger], reqConfig)).json()
        console.log(response)
    }

    const changeProfile = () => {
        if (page === profile.newUser) {
            setpage(profile.oldUser)
            setUi('Sign Up')
        } else {
            setpage(profile.newUser)
            setUi('Sign In')
        }
    }

    // update every time that any input updates it value
    const updateBox = (event) => {
        let obj = userInfo
        let mark = String(event.target.id)
        obj[mark] = event.target.value

        setUserInfo(obj)
    }

    // get all the user data and put it in a object
    const submitUser = () => {
        let userData = userInfo
        userData['creation'] = new Date();
        serverRequest(userData)
    }

    return (
        <>
            <div className="conteiner">
                <h1>{uiChanger}</h1>
                <div className="input_conteiner">
                    {page.map((data) => {
                        return (<Input config={data} func={updateBox} key={data.id} />)
                    })}
                    <div className="button__crud">
                        <button onClick={submitUser} type="submit">
                            Confirm
                        </button>
                    </div>
                </div>
            </div>
            <footer >
                <p className="ui-change" onClick={changeProfile} id="pointer">
                    {uiChanger}?
                </p>
            </footer>
        </>
    )
}


export default NewUser;