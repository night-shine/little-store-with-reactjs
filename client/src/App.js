import { Header } from './components/header/Header';
import Crud from './crud/Crud'

import './App.css';

function App() {
  return (
    <>
    <Header />
    <Crud />
    </>
  );
}

export default App;
