import React from "react";

import "./header-style.css"

import menuIcon from './icons/menu.svg'
import cartIcon from './icons/cart.svg'
import searchIcon from './icons/search/search.svg'

export const Header = (props) => {
    return (
        <div className="main-header">
            <ul>
                <li>
                    <div className="list-conteiner">
                        <img className="icon__navbar" src={menuIcon} alt='menu' />
                    </div>
                </li>
                <li>
                    <div className="list-conteiner">
                        <div className="search_bar">
                            <input className="search__navbar" type='text' placeholder="search" />
                            <img className="icon__navbar" src={searchIcon} alt='search icon'/>
                        </div>

                    </div>
                </li>
                <li>
                    <div className="list-conteiner">
                        <img className="icon__navbar" src={cartIcon} alt='cart' />
                    </div>
                </li>
            </ul>
        </div>
    )
}