# this is the `/createUser` explanation
 --- 
## the basic
this API module uses the [[database]].

- uses the function `insertData()` from [[database]]

> it has a `async function` inside, its required because of the Data Base

----
## how does this works
this works using the `express` module `.get` and listen to the host, with the propretie `/createUser` you can check user information in the database.

-  need connect to the API using `fetch` and post with the user information.
the informatiion needs to be a json like this one;
```json
{
  "user_email": "test@test.teste",
  "user_name": "teste",
  "user_pass": "teste",
  "creation": "Mon Sep 19 2022 18:55:19 GMT-0300 (Brasilia Standard Time)"
}
```

this is required because this method register the user on the database.
- in the response, you can get 2.
1. when the registration id OK.
	```json
	{
	  "status":"successful"
	}
	```
2. when the registration get some issue (when the user exists).
	```json
	{
	  "status":"failed",
	  "reason":"user exists"
	}
	```

---

## graphic
```mermaid
flowchart TD;
	subgraph createUser;
		request(request) --> check{check};
		check --> userExists(User Exists) & ok(User don't Exists)
		ok --write on database--> insertData[register the user]
		userExists & insertData---> res(response)
	end;
	res --> client
```

