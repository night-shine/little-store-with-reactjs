# this is the `/checkUser` explanation
--- 

## the basic
this API module uses the [[database]].

- uses the function `getData()` from [[database]]

> it has a `async function` inside, its required because of the Data Base

---

## how does this works

this works using the `express` module `.get` and listen to the host, with the propretie `/checkUser` you can check user information in the database
-  need connect to the API using `fetch` and post with the user information.
the informatiion needs to be a json like this one;
```json
{
  "user_email":"user@email.user",
  "user_pass":"user password"
}
```

because needs to find the user, in this case it finds by the `user_email`, and check with the `user_pass` with the password of the database
can return 3 possibles json
1. the good one, when everthing is OK.
	```json
	{
	  "login":"successful"
	}
	```
2. one in case of the `user_pass` don't match with the database password
	```json
	{
	  "login":"failed",
	  "reson":"password dont match"
	}
	```
3. one in case that the `user_email` is not finded on the database.
	```json
	{
	  "login":"failed",
	  "reson":"user don't existis"
	}
	```

---

## graphic
```mermaid
flowchart TD;
	subgraph checkUser;
		func(request) --> check{check};
		check -.-> noPass(password don't match) & noUser(user don't existis) & ok(successful)
		noPass & noUser & ok --> res(response)
	end;
	res --> client

```

