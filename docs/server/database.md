# this is the docs of the database

---

> here you'll find how the server works

## for user interation on sign in & sign up
```mermaid

flowchart TD;

    subgraph Server;
        datamanager[Manager Data Base];
        getdata(get data);
        insertdata(insert data);
        database[(DataBase)]

        datamanager <-..-> getdata & insertdata;

        getdata & insertdata <-.-> database
    end;
    
    client([client]) -- request --> Server;
    Server -.response.-> client;
    
```
> this is simple, just 2 functions on back-end one called `inserData` and one called `getData`

### insertData
-  `insertData` is an `async` function, that requires a parameter with all user information
	- the parameter is an `json` like this one;
```json
{
  "user_email": "test@test.teste",
  "user_name": "teste",
  "user_pass": "teste",
  "creation": "Mon Sep 19 2022 18:55:19 GMT-0300 (Brasilia Standard Time)"
}
```

#### this only return a simple json, it can be, good or bad
	
- the good response is when the user registrion was OK, will be like this
```json
{
  "status":"successful"
}
```
- when something happen, it wil return a json like this.
```json
{
  "status":"failed",
  "reason":"user exists"
}
```

---

## getData
- get data is an `async` function, that requires a parameter with some user information
	- the parameter can is like this one;
```json
{
  "user_email":"user@email.user",
  "user_pass":"user password"
}
```

### this response will be good or bad
- the good one is where the user password and the user E-mail are correct;
```json
{
  "login":"successful"
}
```
- the bad one is when the user password and/or the useer E-mail are incorrect;
```json
{
  "login":"failed",
  "reson":"password dont match"
}
```
```json
{
  "login":"failed",
  "reson":"user don't existis"
}
```

--- 